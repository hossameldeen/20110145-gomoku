### Project Description:

-A client-server implementation for the famous abstract strategy game, Gomoku, made as a course project for Software-Engineering-I project 2013/2014.

### Team Data:

-Hossam El-Deen Omar Mahmoud El-Sayyed - 20110145  
-Kerollos Asaad Abd El-Messih - 20110269  
-Ahmed Hamed Sayed Hamed - 20110046  
-Joseph Sobhy Sidky - 20110142  