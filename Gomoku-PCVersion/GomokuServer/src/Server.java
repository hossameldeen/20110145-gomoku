import java.io.*;
import java.net.*;
import java.util.*;

class ClientElement {
	public Socket clientNode;
	public int clientNumber;
	public String name;

	ClientElement(Socket a, int b, String n) {
		clientNode = a;
		clientNumber = b;
		name = n;
	}
}

public class Server {

	private static ServerSocket s;

	public static void main(String[] args) throws IOException {
		ServerGUI frame = new ServerGUI();
		frame.setVisible(true);
		frame.setBounds(0, 0, 400, 400);
		ArrayList<ClientElement> waitingClients = new ArrayList<ClientElement>();
		ArrayList<Boolean> busy = new ArrayList<Boolean>();
		int clientNumber = 1;
		try {
			s = new ServerSocket(4444);
			while (true) {
				Socket clientSocket = s.accept();
				busy.add(false);
				Thread t = new Thread(new ClientWorker(clientSocket,
						waitingClients, busy, clientNumber, frame));
				clientNumber++;
				t.start();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

class ClientWorker implements Runnable {
	private ClientElement client;
	ArrayList<ClientElement> waitingClients;
	ArrayList<Boolean> busy;
	ServerGUI frame;

	public ClientWorker(Socket clientSocket,
			ArrayList<ClientElement> waitingClients, ArrayList<Boolean> busy,
			int cn, ServerGUI update) {
		this.waitingClients = waitingClients;
		this.busy = busy;
		client = new ClientElement(clientSocket, cn, "");
		frame = update;
	}

	public void run() {
		try {
			PrintWriter out = new PrintWriter(
					client.clientNode.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(
					client.clientNode.getInputStream()));
			client.name = null;
			do {
				client.name = in.readLine();
			} while (client.name == null);
			boolean foundOneCompatible = false, isenter = false;
			while (!foundOneCompatible) {
				for (int i = 0; i < waitingClients.size(); ++i) {
					if (client.clientNumber != waitingClients.get(i).clientNumber
							&& !busy.get(waitingClients.get(i).clientNumber - 1)
							&& !busy.get(client.clientNumber - 1)) {
						foundOneCompatible = connectTheClients(
								waitingClients.get(i), client);
					}
					if (foundOneCompatible) {
						busy.set(waitingClients.get(i).clientNumber - 1, true);
						busy.set(client.clientNumber - 1, true);
						waitingClients.remove(i);
						frame.updateGUI(waitingClients);
						break;
					}
				}
				if (busy.get(client.clientNumber - 1))
					break;
				if (!foundOneCompatible) {
					if (!isenter) {
						waitingClients.add(new ClientElement(client.clientNode,
								client.clientNumber, client.name));
						frame.updateGUI(waitingClients);
					}
					out.println("Wait");
				} else
					break;
				isenter = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean connectTheClients(ClientElement n1, ClientElement n2) {
		try {
			PrintWriter out1 = new PrintWriter(n1.clientNode.getOutputStream(),
					true);
			BufferedReader in1 = new BufferedReader(new InputStreamReader(
					n1.clientNode.getInputStream()));
			PrintWriter out2 = new PrintWriter(n2.clientNode.getOutputStream(),
					true);
			BufferedReader in2 = new BufferedReader(new InputStreamReader(
					n2.clientNode.getInputStream()));
			out1.println("1");
			out2.println("2");

			out1.println(n2.name);
			out2.println(n1.name);
			
			(new Thread(new ClientWorker2(out1, in2))).start();
			(new Thread(new ClientWorker2(out2, in1))).start();

			//String localPortOfMaster = in1.readLine();
			//out2.println(localPortOfMaster);

		} catch (Exception e) {
			return false;
		}
		return true;
	}
}

class ClientWorker2 implements Runnable {
	private PrintWriter out;
	private BufferedReader in;
	public ClientWorker2(PrintWriter out, BufferedReader in) {
		this.out = out;
		this.in = in;
	}
	public void run() {
		try {
			String tmp1, tmp2;
			while(true) {
				while((tmp1 = in.readLine()) == null);
				tmp2 = in.readLine();
				out.println(tmp1);
				out.println(tmp2);
			}
		}
		catch (Exception e) {
			
		}
	}
}