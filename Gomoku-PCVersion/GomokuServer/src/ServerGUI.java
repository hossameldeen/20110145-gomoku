import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;

public class ServerGUI extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	private JPanel mainPanel;
	private JLabel waitingList;
	private JButton closeButton;

	ServerGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainPanel = new JPanel();
		mainPanel.setBounds(0, 0, 400, 400);
		setContentPane(mainPanel);
		mainPanel.setLayout(null);

		waitingList = new JLabel();
		waitingList.setBounds(0, 0, 384, 290);
		waitingList.setVisible(true);
		waitingList.setBorder(BorderFactory.createLineBorder(Color.black));
		waitingList.setFont(new Font("Serif", Font.BOLD, 20));
		mainPanel.add(waitingList);

		closeButton = new JButton("Exit");
		closeButton.setBounds(145, 290, 100, 30);
		closeButton.setActionCommand("exit");
		closeButton.setVisible(true);
		closeButton.addActionListener(this);
		closeButton.setFont(new Font("monospaced", Font.BOLD, 25));
		mainPanel.add(closeButton);
	}

	public void updateGUI(ArrayList<ClientElement> waitingClients) {
		String list = "";
		for (int i = 0; i < waitingClients.size(); i++)
			list = list + "   " + waitingClients.get(i).name + "\n";
		waitingList.setText(list);
	}

	public void actionPerformed(ActionEvent event) {
		String command = event.getActionCommand();
		if (command.equals("exit"))
			System.exit(0);
	}
}
