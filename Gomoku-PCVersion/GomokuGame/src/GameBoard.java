import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.*;

import javax.imageio.ImageIO;
import javax.swing.*;

public class GameBoard extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	private JPanel gamePanel, menuPanel;
	private JButton backButton;
	private JButton[][] boardCells = new JButton[19][19];
	private JLabel message;
	private OneToOne logicGame;
	private int player, clientNumber;
	private String clientName = "", otherClientName = "";

	GameBoard(int gameType) throws IOException {
		this.setVisible(true);
		this.setExtendedState(Frame.MAXIMIZED_BOTH);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Create Board Panel And Add Buttons.
		board();
		// Create BackGround Panel And Add Buttons.
		backGround();

		player = 1;
		clientNumber = -1;
		if (gameType == 1) {
			message.setIcon(new ImageIcon("Images/player1.png"));
			logicGame = new OneToOne();
		} else {
			do {
				clientName = (String) JOptionPane.showInputDialog(null,
						"Enter Your Name", "", JOptionPane.PLAIN_MESSAGE);
			} while (clientName.equals(""));
			message.setIcon(new ImageIcon("Images/wait.png"));
			logicGame = new Online(this, clientName);
		}
	}

	public void board() throws IOException {
		gamePanel = new JPanel();
		gamePanel.setBounds(360, 40, 627, 627);
		this.add(gamePanel);
		gamePanel.setLayout(null);
		gamePanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		for (int i = 0; i < 19; i++) {
			for (int j = 0; j < 19; j++) {
				boardCells[i][j] = new JButton("");
				boardCells[i][j].addActionListener(this);
				boardCells[i][j].setVisible(true);
				boardCells[i][j].setBounds((i * 33) + 1, (j * 33) + 1, 30, 30);
				boardCells[i][j].setActionCommand(i + " " + j);
				boardCells[i][j].setContentAreaFilled(false);
				boardCells[i][j].setBorderPainted(false);
				gamePanel.add(boardCells[i][j]);
			}
		}
		BufferedImage myPicture1 = ImageIO.read(new File(
				"Images/boardbackground.jpg"));
		JLabel backGround1 = new JLabel(new ImageIcon(myPicture1));
		backGround1.setBounds(1, 1, 625, 625);
		gamePanel.add(backGround1);
	}

	public void backGround() throws IOException {
		menuPanel = new JPanel();
		menuPanel.setBounds(0, 701, 10, 10);
		this.add(menuPanel);
		menuPanel.setLayout(null);
		menuPanel.setBorder(BorderFactory.createLineBorder(Color.black));

		backButton = new JButton("");
		backButton.setBounds(1270, 650, 99, 58);
		backButton.setIcon(new ImageIcon("Images/back.png"));
		backButton.setActionCommand("back");
		backButton.addActionListener(this);
		backButton.setContentAreaFilled(false);
		backButton.setBorderPainted(false);
		menuPanel.add(backButton);

		message = new JLabel();
		message.setBounds(0, 660, 200, 50);
		message.setVisible(true);
		menuPanel.add(message);

		BufferedImage myPicture2 = ImageIO.read(new File(
				"Images/background.jpg"));
		JLabel backGround2 = new JLabel(new ImageIcon(myPicture2));
		backGround2.setBounds(0, 0, 1366, 768);
		menuPanel.add(backGround2);
	}

	public void actionPerformed(ActionEvent event) {
		String command = event.getActionCommand();
		if (command.equals("back")) {
			try {
				this.setVisible(false);
				Menu frame = new Menu();
				frame.setVisible(true);
				frame.setExtendedState(Frame.MAXIMIZED_BOTH);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (player != -1) {
			int position = command.indexOf(' ');
			Integer x = Integer.parseInt(command.substring(0, position));
			Integer y = Integer.parseInt(command.substring(position + 1));
			int state = logicGame.makeMove(x, y, this);
			if (state <= 2)
				endGame(state);
			else if (state == 3)
				message.setIcon(new ImageIcon("Images/invaildmove.png"));
			else if (state == 5)
				message.setIcon(new ImageIcon("Images/wait.png"));
		}
	}

	public void updatePosition(int x, int y) {
		boardCells[x][y].setContentAreaFilled(true);
		if (player == 1)
			boardCells[x][y].setIcon(new ImageIcon("Images/black3.jpg"));
		else
			boardCells[x][y].setIcon(new ImageIcon("Images/white3.jpg"));
		player = 3 - player;
		if (player == 1)
			message.setIcon(new ImageIcon("Images/player1.png"));
		else
			message.setIcon(new ImageIcon("Images/player2.png"));
	}

	public void endGame(int state) {
		if (state <= 1) {
			if (3 - player == 1)
				message.setIcon(new ImageIcon("Images/player1won.png"));
			else
				message.setIcon(new ImageIcon("Images/player2won.png"));
		} else
			message.setIcon(new ImageIcon("Images/draw.png"));
		player = -1;
	}

	public void setOtherClientName(String otherName) {
		otherClientName = otherName;
		String output = clientName + " and his number is " + clientNumber
				+ " Vs " + otherClientName + " and his number is "
				+ (3 - clientNumber);
		JOptionPane.showMessageDialog(null, output);
		if (player == 1)
			message.setIcon(new ImageIcon("Images/player1.png"));
		else
			message.setIcon(new ImageIcon("Images/player2.png"));
	}

	public void setPlayerNumber(int number) {
		clientNumber = number;
	}
}
