import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;

import javax.imageio.*;
import javax.swing.*;

public class Menu extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	private JPanel mainPanel;
	private JButton gameName;
	private JButton startButton;
	private JButton exitButton;
	private JButton oneToOneButton;
	private JButton onlineButton;
	private JButton backButton;
	private JLabel backGround;
	private int screenWidth, screenHeight;

	Menu() throws IOException {
		Toolkit tk = Toolkit.getDefaultToolkit();
		screenWidth = ((int) tk.getScreenSize().getWidth());
		screenHeight = ((int) tk.getScreenSize().getHeight());

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainPanel = new JPanel();
		setContentPane(mainPanel);
		mainPanel.setLayout(null);

		defineGameNameButton();
		defineStartButton();
		defineExitButton();
		defineOnlineButton();
		defineOneToOneButton();
		defineBackButton();

		BufferedImage myPicture = ImageIO
				.read(new File("Images/background.jpg"));
		backGround = new JLabel(new ImageIcon(myPicture));
		backGround.setBounds(0, 0, screenWidth, screenHeight);
		mainPanel.add(backGround);

		setMainMenuVisible();
	}

	public void defineGameNameButton() {
		gameName = new JButton("");
		gameName.setBounds((screenWidth / 2) - 135, (screenHeight / 3), 263, 92);
		gameName.setIcon(new ImageIcon("Images/gamename.png"));
		gameName.setVisible(true);
		gameName.setContentAreaFilled(false);
		gameName.setBorderPainted(false);
		mainPanel.add(gameName);
	}

	public void defineStartButton() {
		startButton = new JButton("");
		startButton.setBounds((screenWidth / 2) - 100, (screenHeight / 3) + 110,
				200, 60);
		startButton.setIcon(new ImageIcon("Images/start.png"));
		startButton.setActionCommand("start");
		startButton.setVisible(false);
		startButton.addActionListener(this);
		startButton.setContentAreaFilled(false);
		startButton.setBorderPainted(false);
		mainPanel.add(startButton);
	}

	public void defineExitButton() {
		exitButton = new JButton("");
		exitButton.setBounds((screenWidth / 2) - 100, (screenHeight / 3) + 165,
				200, 60);
		exitButton.setIcon(new ImageIcon("Images/quit.png"));
		exitButton.setActionCommand("exit");
		exitButton.setVisible(false);
		exitButton.addActionListener(this);
		exitButton.setContentAreaFilled(false);
		exitButton.setBorderPainted(false);
		mainPanel.add(exitButton);
	}

	public void defineOneToOneButton() {
		oneToOneButton = new JButton("");
		oneToOneButton.setBounds((screenWidth / 2) - 100,
				(screenHeight / 3) + 110, 200, 60);
		oneToOneButton.setIcon(new ImageIcon("Images/2player.png"));
		oneToOneButton.setActionCommand("one-to-one");
		oneToOneButton.setVisible(false);
		oneToOneButton.addActionListener(this);
		oneToOneButton.setContentAreaFilled(false);
		oneToOneButton.setBorderPainted(false);
		mainPanel.add(oneToOneButton);
	}

	public void defineOnlineButton() {
		onlineButton = new JButton("");
		onlineButton.setBounds((screenWidth / 2) - 100,
				(screenHeight / 3) + 165, 200, 60);
		onlineButton.setIcon(new ImageIcon("Images/online.png"));
		onlineButton.setActionCommand("online");
		onlineButton.setVisible(false);
		onlineButton.addActionListener(this);
		onlineButton.setContentAreaFilled(false);
		onlineButton.setBorderPainted(false);
		mainPanel.add(onlineButton);
	}

	public void defineBackButton() {
		backButton = new JButton("");
		backButton.setBounds((screenWidth / 2) - 100, (screenHeight / 3) + 220,
				200, 60);
		backButton.setIcon(new ImageIcon("Images/back.png"));
		backButton.setActionCommand("back");
		backButton.addActionListener(this);
		backButton.setVisible(false);
		backButton.setContentAreaFilled(false);
		backButton.setBorderPainted(false);
		mainPanel.add(backButton);
	}

	public void setMainMenuVisible() {
		gameName.setVisible(true);
		startButton.setVisible(true);
		exitButton.setVisible(true);
	}

	public void setMainMenuUnVisible() {
		startButton.setVisible(false);
		exitButton.setVisible(false);
	}

	public void setGameMenuVisible() {
		gameName.setVisible(true);
		oneToOneButton.setVisible(true);
		onlineButton.setVisible(true);
		backButton.setVisible(true);
	}

	public void setGameMenuUnVisible() {
		gameName.setVisible(false);
		oneToOneButton.setVisible(false);
		onlineButton.setVisible(false);
		backButton.setVisible(false);
	}

	public void actionPerformed(ActionEvent event) {
		String command = event.getActionCommand();
		if (command.equals("start")) {
			setMainMenuUnVisible();
			setGameMenuVisible();
		} else if (command.equals("exit")) {
			System.exit(0);
		} else if (command.equals("one-to-one")) {
			this.setVisible(false);
			try {
				@SuppressWarnings("unused")
				GameBoard gameBoard = new GameBoard(1);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (command.equals("online")) {
			this.setVisible(false);
			try {
				@SuppressWarnings("unused")
				GameBoard gameBoard = new GameBoard(2);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (command.equals("back")) {
			setGameMenuUnVisible();
			setMainMenuVisible();
		}
	}
}