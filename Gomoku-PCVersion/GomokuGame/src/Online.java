import java.io.*;
import java.net.*;

import javax.swing.SwingWorker;

public class Online extends OneToOne {

	Socket server;
	PrintWriter sentMove;
	BufferedReader takeMove;
	String player = "", name = "", otherName = "";
	GameBoard temp;

	Online(GameBoard updateMoveInGUIBoard, String clientName) {
		temp = updateMoveInGUIBoard;
		logic = new GameLogic();
		name = clientName;
		new RaceWorker1().execute();
	}

	class RaceWorker1 extends SwingWorker<Void, Void> {
		protected Void doInBackground() throws Exception {
			try {
				server = new Socket("localhost", 4444);
				PrintWriter out = new PrintWriter(server.getOutputStream(),
						true);
				BufferedReader in = new BufferedReader(new InputStreamReader(
						server.getInputStream()));
				out.println(name);
				while ((player = in.readLine()).equals("Wait")) {
				}
				while ((otherName = in.readLine()).equals("Wait")) {
				}
				temp.setPlayerNumber((int) (player.charAt(0) - '0'));
				temp.setOtherClientName(otherName);
				sentMove = out;
				takeMove = in;
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (player.equals("2"))
				new RaceWorker2().execute();
			return null;
		}
	}

	class RaceWorker2 extends SwingWorker<Void, Void> {
		protected Void doInBackground() throws Exception {
			try {
				while (!takeMoveServer()) {
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
	}

	public int makeMove(int x, int y, GameBoard updateMoveInGUIBoard) {
		if (player.equals("Wait"))
			return 5;
		if ((logic.getTurn() == 0 && player.equals("2"))
				|| (logic.getTurn() == 1 && player.equals("1")))
			return 3;
		int state = super.makeMove(x, y, updateMoveInGUIBoard);
		if (state == 3)
			return 3;
		try {
			sentMoveServer(x, y);
		} catch (IOException e) {
			e.printStackTrace();
		}
		new RaceWorker2().execute();
		return state;
	}

	public void sentMoveServer(int x, int y) throws IOException {
		if ((player.equals("1") && logic.getTurn() == 1)
				|| (player.equals("2") && logic.getTurn() == 0)) {
			String tm1 = "";
			while (x != 0) {
				tm1 = (char) ((x % 10) + '0') + tm1;
				x /= 10;
			}
			sentMove.println(tm1);
			String tm2 = "";
			while (y != 0) {
				tm2 = (char) ((y % 10) + '0') + tm2;
				y /= 10;
			}
			sentMove.println(tm2);
		}
	}

	public boolean takeMoveServer() throws IOException {
		if ((player.equals("1") && logic.getTurn() == 0)
				|| (player.equals("2") && logic.getTurn() == 1)) {
			return true;
		}
		String ix = takeMove.readLine();
		String iy = takeMove.readLine();
		if (ix == null || iy == null)
			return false;
		int x = Integer.parseInt(ix);
		int y = Integer.parseInt(iy);
		int state = super.makeMove(x, y, temp);
		if (state <= 2)
			temp.endGame(state);
		return true;
	}
}
