public class OneToOne {

	protected GameLogic logic;

	OneToOne() {
		logic = new GameLogic();
	}

	public int makeMove(int x, int y, GameBoard updateMoveInGUIBoard) {
		if (logic.makeMove(x, y)) {
			updateMoveInGUIBoard.updatePosition(x, y);
			int gameState = logic.gameState();
			if (gameState <= 2)
				return gameState;
		} else
			return 3;
		return 4;
	}

}
