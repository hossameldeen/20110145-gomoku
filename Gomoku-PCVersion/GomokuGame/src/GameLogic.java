public class GameLogic {

	private int[][] boardCells;
	private int playerTurn;
	private int boardSize = 19;

	GameLogic() {
		playerTurn = 0;
		boardCells = new int[boardSize][boardSize];
		for (int i = 0; i < boardSize; i++)
			for (int j = 0; j < boardSize; j++)
				boardCells[i][j] = -1;
	}

	public boolean makeMove(int x, int y) {
		if (boardCells[x][y] != -1)
			return false;
		boardCells[x][y] = playerTurn;
		return true;
	}

	public int getTurn() {
		return playerTurn;
	}

	public int gameState() {
		playerTurn = 1 - playerTurn;
		for (int i = 0; i < boardSize; i++) {
			for (int j = 0; j < boardSize; j++) {
				Boolean c = true;
				if (j + 5 <= boardSize) {
					for (int k = 0; k < 5; k++)
						if (boardCells[i][j + k] != 1 - playerTurn)
							c = false;
					if (c)
						return 1 - playerTurn;
				}
				if (i + 5 <= boardSize) {
					c = true;
					for (int k = 0; k < 5; k++)
						if (boardCells[i + k][j] != 1 - playerTurn)
							c = false;
					if (c)
						return 1 - playerTurn;
				}
				if (i + 5 <= boardSize && j + 5 <= boardSize) {
					c = true;
					for (int k = 0; k < 5; k++)
						if (boardCells[i + k][j + k] != 1 - playerTurn)
							c = false;
					if (c)
						return 1 - playerTurn;
				}
				if (i + 5 <= boardSize && j - 4 >= 0) {
					c = true;
					for (int k = 0; k < 5; k++)
						if (boardCells[i + k][j - k] != 1 - playerTurn)
							c = false;
					if (c)
						return 1 - playerTurn;
				}
			}
		}
		boolean draw = true;
		for (int i = 0; i < boardSize; i++)
			for (int j = 0; j < boardSize; j++)
				if (boardCells[i][j] == -1)
					draw = false;
		if (draw)
			return 2;
		return 3;
	}
}
