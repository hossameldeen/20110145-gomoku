package com.example.gomokugame;


import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

public class MainActivity extends Activity 
{
	public GridView gridview;
	public GameBoard gameBoard;
	public TextView textView;
	public Button newGameButton;
	public Button exit;
	
	protected void onCreate(Bundle savedInstanceState) 
	{
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		gameBoard = new GameBoard(this);
		textView = (TextView) findViewById(R.id.textView1);
		textView.setText("Player 1 Turn");
		
		gridview = (GridView) findViewById(R.id.gridview);
		gridview.setAdapter(gameBoard);
		gridview.setOnItemClickListener( new OnItemClickListener() {
		public void onItemClick(AdapterView<?> parent, View v, int pos,long id) {
				if(gameBoard.player!=-1)
				{
				   int stat=gameBoard.makeMove(pos);
				   gridview.setAdapter(gameBoard);
				   if(stat<=2) endGame(stat);
				   else if(stat==4) 
				   {
					   textView.setText("Player " + gameBoard.player  + " Turn");
				   }
				}
			}
        });
		
		newGameButton = (Button) findViewById(R.id.button1);
		newGameButton.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				gameBoard = new GameBoard( MainActivity.this );
				textView = (TextView) findViewById(R.id.textView1);
				textView.setText("Player 1 Turn");
				gridview = (GridView) findViewById(R.id.gridview);
				gridview.setAdapter(gameBoard);
			}
		});
		
		exit = (Button) findViewById(R.id.button2);
		exit.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				finish();	
			}
		});
	}	

	public void endGame(int state) 
	{   
		if (state <= 1) {
			if (3 - gameBoard.player == 1) textView.setText("Player 1 Win");
			else  textView.setText("Player 2 Win");
		} 
		else  textView.setText("Draw Game");
		gameBoard.player = -1;
	}
}
