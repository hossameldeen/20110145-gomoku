package com.example.gomokugame;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class GameBoard extends BaseAdapter 
{
	Context myContext;
	Integer[] arr;
	int boardSize=8;
	OneToOne newGame;
	int player;
	
	public  GameBoard()
	{
		player=1;
		newGame = new OneToOne();
		arr = new Integer[boardSize*boardSize];	
		for(int i=0;i<boardSize*boardSize;i++) 
			arr[i]=R.drawable.blank;
	}
	public  GameBoard(Context c)
	{
		player=1;
		newGame = new OneToOne();
		myContext=c;
		arr = new Integer[boardSize*boardSize];	
		for(int i=0;i<boardSize*boardSize;i++) 
			arr[i]=R.drawable.blank;
	}

    public int makeMove(int pos)
    {
    	int x=pos/boardSize  , y = pos%boardSize;
    	int state=newGame.makeMove(x,y,this);
    	if(state==3) return state; // invalid move
    	
    	if(player==1) arr[pos]=R.drawable.black;
    	else arr[pos]=R.drawable.white;
    	player = 3 - player;
    	return state;
    }
    
	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		ImageView imageV ;
		if(convertView==null)
		{
			imageV = new ImageView(myContext);
			imageV.setLayoutParams( new GridView.LayoutParams(40,40) );
			imageV.setScaleType(ImageView.ScaleType.FIT_CENTER);
			imageV.setPadding(1, 1, 1, 1);
		}
		else imageV = (ImageView) convertView;
		imageV.setImageResource(arr[pos]);
		return imageV;
	}
    
	@Override
	public int getCount() {
		return arr.length;
	}

	@Override
	public Object getItem(int pos) {
		return arr[pos];
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}
}
