package com.example.gomokugame;

public class OneToOne {
	
	protected GameLogic logic;

	OneToOne() {
		logic = new GameLogic();
	}
	public int makeMove(int x, int y, GameBoard updateMoveInGUIBoard) {
		/*
		return  
				0 : player1 win
				1 : player2 win
				2 : draw game
				3 : invalid
				4 : continue 
		*/
		if (logic.makeMove(x, y)){
			int gameState = logic.gameState();
			if (gameState <= 2)
				return gameState;
		} else  return 3;
		
		return 4;
	}
}
