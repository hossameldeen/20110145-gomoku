package gomokuGWT.server;


/**
 * This class represents the abstract logic of the game.
 * It has nothing to do with servers / GUI, ... etc.
 * Please, create a new instance for each game.
 * 
 * You can track the game either by tracking
 * the current turn OR by tracking cells states.
 * @author 7
 *
 */
public class GameLogic {
	
	private int nRows, nCols;
	private int[][] boardCells;
	/*
	 * Representing who should play now
	 */
	private int whoHasTheTurn;
	/*
	 * For the purpose a recursive function
	 */
	private int di[] = {0, 0, 1, -1, 1, 1, -1, -1};
	private int dj[] = {1, -1, 0, 0, 1, -1, 1, -1};
	
	/*
	 * It's just recursion to get number of contiguous
	 * cells as described by the game conditions .. 7aga
	 * zay keda.
	 * Returns number of possibly-winning cells INCLUDING
	 * boardCells[x][y]
	 * 
	 * I wrote it as stupid as I could.
	 */
	private int getNumberOfCells(int x, int y, int dir, int color, boolean[][][] vis)
	{
		if (x < 0 || y < 0 || x >= nRows || y >= nCols || vis[x][y][dir] || boardCells[x][y] != color)
			return 0;
		vis[x][y][dir] = true;
		int max = 0;
		int temp = getNumberOfCells(x + di[dir], y + dj[dir], dir, color, vis);
		if (temp > max)
			max = temp;
		return 1 + max;
	}
	
	/**
	 * Constants used to represent two things:
	 * 		- Whose turn it is: WHITE & BLACK. It
	 * 			returns a turn EVEN IF GAME HAS ENDED.
	 * 		- Cell state: WHITE & BLACK & FREE
	 * There're 2 turns only
	 */
	public static final int FREE = 0;
	public static final int WHITE = 1;
	public static final int BLACK = 2;
	
	/**
	 * Constants used to represent state of the game.
	 * RUNNING: You can still make moves
	 * WHITE_WON, BLACK_WON: Obvious
	 * DRAW: Game board full but no player achieved win condition.
	 */
	public static final int RUNNING = 0;
	public static final int WHITE_WON = 1;
	public static final int BLACK_WON = 2;
	public static final int DRAW = 3;
	
	/**
	 * Calls GameLogic(10, 10)
	 */
	public GameLogic()
	{
		this(10, 10);
	}
	/**
	 * Preconditions:
	 * 		- Dimensions are positive
	 * Postconditions:
	 * 		- A FREE game board with specified dimensions
	 * 			is created
	 * 		- WHITE has the turn
	 * 		- getGameState() should return RUNNING
	 * 
	 */
	public GameLogic(int nRows, int nCols)
	{
		this.nRows = nRows;
		this.nCols = nCols;
		boardCells = new int[nRows][nCols];
		for (int i = 0; i < nRows; ++i)
			for (int j = 0; j < nCols; ++j)
				boardCells[i][j] = FREE;
		whoHasTheTurn = WHITE;
	}
	/**
	 * Preconditions:
	 * Assumes validaty of parameters.
	 * Parameters should be 0-based indexes
	 * 
	 * Posconditions:
	 * Board, turn, or anything else related to game logic
	 * is updated.
	 * 
	 * @return true iff move was made
	 * 		   false iff move wasn't made (due to any reason
	 * 						like game has ended or cell isn't free)
	 */
	public boolean makeMove(int x, int y)
	{
		//System.out.println(x + " " + y + " " + getGameState() + " " + boardCells[x][y]);
		if (getGameState() != RUNNING || boardCells[x][y] != FREE)
			return false;
		boardCells[x][y] = whoHasTheTurn;
		whoHasTheTurn = 3 - whoHasTheTurn;
		return true;
	}
	/**
	 * Computes current game state.
	 * 
	 * @return one of the class defined constants as described:
	 * RUNNING: You can still make moves
	 * FIRST_WON, SECOND_WON: Obvious
	 * DRAW: Game board full but no player achieved win condition.
	 */
	public int getGameState()
	{
		boolean tempVis[][][] = new boolean[nRows][nCols][8];
		boolean draw = true;
		for (int i = 0; i < nRows; ++i)
			for (int j = 0; j < nCols; ++j) {
				if (boardCells[i][j] == FREE) {
					draw = false;
					continue;	//It shouldn't achieve win condition
				}
				for (int k = 0; k < di.length; ++k) {
					int temp = getNumberOfCells(i, j, k, boardCells[i][j], tempVis);
					if (temp == 5)
						return 3 - whoHasTheTurn;
				}
			}
		if (draw)
			return DRAW;
		return RUNNING;
	}
	/**
	 * Returns who should play next.
	 * Doesn't differentiate between a RUNNING or otherwise game.
	 * 
	 * @return one of the class defined constants:
	 * WHITE
	 * BLACK
	 */
	public int getTurn()
	{
		return whoHasTheTurn;
	}
	/**
	 * Preconditions:
	 * 		- 0-based indexes
	 * 
	 * NO BOUNDARY CHECKING IS DONE
	 * @return: one of the class defined constants:
	 * FREE
	 * WHITE
	 * BLACK
	 */
	public int getCellState(int x, int y)
	{
		return boardCells[x][y];
	}
}
