package gomokuGWT.server;

/**
 * This class is responsible for dealing with both
 * the game board and the game logic.
 * It sends moves from game board to game logic,
 * then changes game board accordingly.
 * @author 7
 *
 */
public class GameManager {

	protected GameLogic gameLogic;

	/**
	 * Creates a new instance of GameLogic to
	 * keep update of the game state
	 */
	public GameManager()
	{
		gameLogic = new GameLogic();
	}

	/**
	 * Precondition: Assuming correctness of input
	 * 				Indexes are 0-based.
	 * 
	 * @return a string representing the message
	 * 			to be sent to the game board. 
	 */
	public String makeMove(int x, int y)
	{
		String res = "";
		if (gameLogic.makeMove(x, y)) {
			if (gameLogic.getTurn() == GameLogic.BLACK)
				res = x + " " + y + " " + GameLogic.WHITE;
			else
				res = x + " " + y + " " + GameLogic.BLACK;
			int gameState = gameLogic.getGameState();
			if (gameState != GameLogic.RUNNING)
				res += " " + gameState;
		}
		return res;
	}

}

