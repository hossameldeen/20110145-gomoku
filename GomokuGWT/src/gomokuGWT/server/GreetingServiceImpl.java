package gomokuGWT.server;

import gomokuGWT.client.GreetingService;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class GreetingServiceImpl extends RemoteServiceServlet implements
		GreetingService {
	
	GameManager gameManager;

	/**
	 * - Assuming correct input
	 * 
	 * Input					Response
	 * "Create local"			""
	 * "<x> <y>"				Either: "<x> <y> <piece>"
	 * 							OR:		"<x> <y> <piece> <gameState>"
	 * 							OR:		""
	 */
	public String greetServer(String input) throws IllegalArgumentException {
		
		if (input.equals("Create local")) {
			gameManager = new GameManager();
			return "";
		}
		/*else if (input.equals("Create online"))
			gameManager = new OnlineGameManager;*/
		String[] temp = input.split(" ");
		int x = Integer.parseInt(temp[0]), y = Integer.parseInt(temp[1]);
		
		return gameManager.makeMove(x, y);
	}
}
