package gomokuGWT.client;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
/**
 * This class is responsible for parsing the response
 * coming from the GWT response upon game creation.
 * @author 7
 *
 */
public class CreationResponseHandler implements AsyncCallback<String> {
	
	@Override
	public void onFailure(Throwable caught)
	{
		Window.alert("Something wrong happened. Please, contact the developers");
	}

	@Override
	/**
	 * In local case: Nothing expected
	 * In online case: Still to be done
	 */
	public void onSuccess(String res) {
	}

}
