package gomokuGWT.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class GomokuGWT implements EntryPoint {

	/**
	 * Create a remote service proxy to talk to the server-side Greeting service.
	 */
	private final GreetingServiceAsync greetingService = GWT
			.create(GreetingService.class);

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {

		Button newLocalGame = new Button("New Local Game!", new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				greetingService.greetServer("Create local", new CreationResponseHandler());
				new GameBoard(greetingService);
				RootPanel.get("newLocalGame").setVisible(false);
				RootPanel.get("newOnlineGame").setVisible(false);
			}
		});
		Button newOnlineGame = new Button("New Online Game!", new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				/*greetingService.greetServer("Create online", new CreationResponseHandler());
				new GameBoard(greetingService);*/
			}
		});
		
		newLocalGame.setStyleName("gwt-Button");
		newOnlineGame.setStyleName("gwt-Button");
		
		RootPanel.get("newLocalGame").add(newLocalGame);
		RootPanel.get("newOnlineGame").add(newOnlineGame);
	}
}
