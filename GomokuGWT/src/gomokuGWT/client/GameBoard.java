package gomokuGWT.client;

import gomokuGWT.server.GameLogic;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Represents the shown board for a game.
 * All runs on client side.
 * @author 7
 *
 */
public class GameBoard {
	
	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1L;
	private int nRows, nCols;
	
	private VerticalPanel vPanel;
	private HorizontalPanel[] hPanels;
	private GomokuButton[][] boardCells;
	
	public GameBoard(GreetingServiceAsync greetingService)
	{
		this(10, 10, greetingService);
	}
	/**
	 * Shows the game board
	 * @param nRows
	 * @param nCols
	 */
	public GameBoard(int nRows, int nCols, final GreetingServiceAsync greetingService)
	{
		vPanel = new VerticalPanel();
		vPanel.setStyleName("gwt-VerticalPanel");
		
		hPanels = new HorizontalPanel[nRows];
		boardCells = new GomokuButton[nRows][nCols];
		for (int i = 0; i < nRows; ++i) {
			hPanels[i] = new HorizontalPanel();
			for (int j = 0; j < nCols; ++j) {
				boardCells[i][j] = new GomokuButton();
				//boardCells[i][j].setStyleName("gwt-Button");
				boardCells[i][j].x = i;
				boardCells[i][j].y = j;
				boardCells[i][j].gameBoard = this;
				boardCells[i][j].setHTML(("<img style='border:0px' src='Images/blank.png' />"));
				
				boardCells[i][j].addClickHandler(new ClickHandler() {
					
					@Override
					public void onClick(ClickEvent event) {
						final GomokuButton callerButton = (GomokuButton)(event.getSource());
						
						greetingService.greetServer(callerButton.x + " " + callerButton.y, 
						new AsyncCallback<String>() {
							public void onFailure(Throwable caught) {
								final DialogBox dialogBox = new DialogBox();
								dialogBox.setText("Something wrong happened. Please, contact the developers");
								dialogBox.setAnimationEnabled(true);
								dialogBox.center();
							}

							public void onSuccess(String res) {
								if (res.equals(""))
									return;
								String[] temp = res.split(" ");
								int x = Integer.parseInt(temp[0]), y = Integer.parseInt(temp[1]),
										playerNumber = Integer.parseInt(temp[2]);
								callerButton.gameBoard.updateCell(x, y, playerNumber);
								if (temp.length == 4) {
									int gameState = Integer.parseInt(temp[3]);
									callerButton.gameBoard.endGame(gameState);
								}
							}
						});
					}
				});
				
				hPanels[i].add(boardCells[i][j]);
			}
			vPanel.add(hPanels[i]);
		}
		
		RootPanel.get("gameBoard").add(vPanel);
	}
	/**
	 * Assumes validaty of parameters
	 * @param playerNumber It should be
	 * 			GameLogic.WHITE or GameLogic.BLACK ONLY
	 */
	public void updateCell(int x, int y, int playerNumber)
	{
		if (playerNumber == GameLogic.WHITE)
			boardCells[x][y].setHTML(("<img style='border:0px' src='Images/white.png' />"));
		else
			boardCells[x][y].setHTML(("<img style='border:0px' src='Images/black.png' />"));
		boardCells[x][y].setEnabled(false);
	}
	/**
	 * 
	 * @param gameState: It should be either:
	 * 						- GameLogic.WHITE_WON
	 * 						- GameLogic.BLACK_WON
	 * 						- GameLogic.DRAW
	 */
	public void endGame(int gameState)
	{
		//Disabling the buttons
		for (int i = 0; i < nRows; ++i)
			for (int j = 0; j < nCols; ++j)
				boardCells[i][j].setEnabled(false);
		
		// Create the popup dialog box
		final DialogBox announcer = new DialogBox();
		if (gameState == GameLogic.WHITE_WON)
			announcer.setText("White Won!");
		else if (gameState == GameLogic.BLACK_WON)
			announcer.setText("Black Won!");
		else if (gameState == GameLogic.DRAW)
			announcer.setText("Game has ended in draw.");
		else
			announcer.setText("Seems like something wrong happened\n" +
					  "I'd advise you to close and open the page");

		//Shewayet sho3'l dayya2
		announcer.setAnimationEnabled(true);
		announcer.setStyleName("gwt-DialogBox");
		final Button closeButton = new Button("Remove Board");
		VerticalPanel dialogVPanel = new VerticalPanel();
		dialogVPanel.add(closeButton);
		announcer.setWidget(dialogVPanel);
		announcer.center();
		
		// Add a handler to close the DialogBox
		closeButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				announcer.hide();
				RootPanel.get("gameBoard").remove(vPanel);
				RootPanel.get("newLocalGame").setVisible(true);
				RootPanel.get("newOnlineGame").setVisible(true);
			}
		});
	}
}
