package gomokuGWT.client;

import com.google.gwt.user.client.ui.Button;

/**
 * Extends GWT Button.
 * Just made to add variables to be used in onClick
 * function to know some properties of this button
 * It's not worth making setters and getters for it.
 * Just don't edit them after setting them, please.
 * Will add only inherited constructors when needed
 * @author 7
 *
 */
public class GomokuButton extends Button {
	/**
	 * Does nothing other than Button(String) does.
	 * Stupid Java :( Why do I have to create it? Isn't that
	 * a waste of my precious time?
	 * @param string
	 */
	public GomokuButton(String string) {
		super(string);
	}
	
	public GomokuButton() {
		
	}

	public int x, y;
	GameBoard gameBoard;
}
